# Copyright (C) 2022 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import("//build/ohos.gni")
import("//developtools/profiler/build/config.gni")

ohos_shared_library("vorbis") {
  vorbis_srcs = [
    "mdct.c",
    "smallft.c",
    "block.c",
    "envelope.c",
    "window.c",
    "lsp.c",
    "lpc.c",
    "analysis.c",
    "synthesis.c",
    "psy.c",
    "info.c",
    "floor1.c",
    "floor0.c",
    "res0.c",
    "mapping0.c",
    "registry.c",
    "codebook.c",
    "sharedbook.c",
    "lookup.c",
    "bitrate.c",
  ]
  sources = []
  foreach(src, vorbis_srcs) {
    sources += [ "./lib/" + src ]
  }
  public_deps = [ "//third_party/libogg:libogg"]

  cflags = [
    "-D_GNU_SOURCE",
    "-D_HAS_EXCEPTIONS=0",
    "-DHAVE_CONFIG_H",
    "-Wno-macro-redefined",
    "-Wno-unused-variable",
  ]
  ldflags = []
  if (target_os == "win") {
    cflags += [
      "-DNOMINMAX",
      "-D_CRT_SECURE_NO_DEPRECATE",
      "-D_CRT_NONSTDC_NO_DEPRECATE",
      "-D_WIN32_WINNT=0x0600",
    ]
    ldflags += [ "-defaultlib:ws2_32.lib" ]
  }

  defines = [ "VORBIS_SHAREDLIB" ]

  include_dirs = [
    ".",
    "./include",
  ]
  if (target_os == "android" || target_os == "ohos" || target_os == "aosp") {
    include_dirs += [ "config_android" ]
  } else if (target_os == "linux") {
    include_dirs += [ "config_linux" ]
  } else if (target_os == "win") {
    include_dirs += [ "config_windows" ]
  }

  # print("cares_target_os is $target_os")
  install_enable = true
  subsystem_name = "${OHOS_PROFILER_SUBSYS_NAME}"
  part_name = "${OHOS_PROFILER_PART_NAME}"
}

ohos_shared_library("vorbisenc") {
  vorbisenc_srcs = [
    "vorbisenc.c"
  ]
  sources = []
  foreach(src, vorbisenc_srcs) {
    sources += [ "./lib/" + src ]
  }
  public_deps = [ "//third_party/vorbis:vorbis" ]
  cflags = [
    "-D_GNU_SOURCE",
    "-D_HAS_EXCEPTIONS=0",
    "-DHAVE_CONFIG_H",
    "-Wno-macro-redefined",
  ]
  defines = [ "VORBISENC_SHAREDLIB" ]

  include_dirs = [
    "./",
    "./lib",
    "./include",
  ]
  if (target_os == "android" || target_os == "ohos" || target_os == "aosp") {
    include_dirs += [ "config_android" ]
  } else if (target_os == "linux") {
    include_dirs += [ "config_linux" ]
  } else if (target_os == "win") {
    include_dirs += [ "config_windows" ]
  }

  # print("cares_target_os is $target_os")
  install_enable = true
  subsystem_name = "${OHOS_PROFILER_SUBSYS_NAME}"
  part_name = "${OHOS_PROFILER_PART_NAME}"
}

ohos_shared_library("vorbisfile") {
  vorbisfile_srcs = [
    "vorbisfile.c"
  ]
  sources = []
  foreach(src, vorbisfile_srcs) {
    sources += [ "./lib/" + src ]
  }
  public_deps = [ "//third_party/vorbis:vorbis" ]
  cflags = [
    "-D_GNU_SOURCE",
    "-D_HAS_EXCEPTIONS=0",
    "-DHAVE_CONFIG_H",
    "-Wno-macro-redefined",
    "-Wno-unused-variable",
  ]
  defines = [ "VORBISFILE_SHAREDLIB" ]

  include_dirs = [
    "./",
    "./lib",
    "./include",
  ]
  if (target_os == "android" || target_os == "ohos" || target_os == "aosp") {
    include_dirs += [ "config_android" ]
  } else if (target_os == "linux") {
    include_dirs += [ "config_linux" ]
  } else if (target_os == "win") {
    include_dirs += [ "config_windows" ]
  }

  # print("cares_target_os is $target_os")
  install_enable = true
  subsystem_name = "${OHOS_PROFILER_SUBSYS_NAME}"
  part_name = "${OHOS_PROFILER_PART_NAME}"
}


ohos_executable("vorbis_test") {
  sources = [ 
    "./test/test.c",
    "./test/util.c",
    "./test/write_read.c",
  ]
  deps = [
          ":vorbisfile",
          ":vorbisenc",
  ]
  include_dirs = [
    "./",
    "./lib",
    "./include",
    "./test",
  ]
#  shared_link = true
  subsystem_name = "${OHOS_PROFILER_SUBSYS_NAME}"
  part_name = "${OHOS_PROFILER_PART_NAME}"
}

#group("hiperf_target_all") {
#  deps = [":hiperf_host(//build/toolchain/ohos:ohos_clang_arm)"]
#}
